# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#   
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Major.create(:name => 'Daley', :city => cities.first)
User.create(:name => 'User 1')
User.create(:name => 'User 2')
User.create(:name => 'User 3')

Book.create(:title => 'Book 1')
Book.create(:title => 'Book 2')
Book.create(:title => 'Book 3')

Movie.create(:title => 'Movie 1')
Movie.create(:title => 'Movie 2')
Movie.create(:title => 'Movie 3')