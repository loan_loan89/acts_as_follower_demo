# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_follow_session',
  :secret      => '8130467583ef69ba2fc1d64964c79cc40af1af54fb48d344769e4a2ead7797022ee9b04e0491671c513576fcbe1cc83bfde8e70a3b0ef98101150adef0a75a52'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
